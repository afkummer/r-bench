for sz in "10,30, 50, 70"
do
   for th in 8 16 24 32 40 48
   do
      export THREADS=$th
      export SIZES="c("$sz")"
      Rscript caretParallel.R
   done
done
